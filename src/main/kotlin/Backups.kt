import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import kotlin.io.path.Path
import kotlin.io.path.copyTo
import kotlin.io.path.deleteIfExists
import kotlin.io.path.exists
/**
* ferBackup -> fa backup de l'arxiu d'usuaris amb el format [dataActual]userData.txt. Guarda aquest backup a la carpeta backups.
* Controla si ja existeix un backup.
* */
fun ferBackup(){
    val dataActual = agafarDataActual()
    //Establir directori d'origen i destí i nom de fitxer
    val source = Path("./src/data/userData.txt")
    val target = Path("./src/backups/${dataActual}userData.txt")
    //Fer còpia de seguretat de userdata a YYYY-MM-DDuserdata.txt
    if (target.exists()){
        println("Ja existeix el backup d'avui, refent el backup.")
        target.deleteIfExists()
    }
    println("------------- BACKUP FET! -------------")
    source.copyTo(target)
}
/**
* agafarDataActual -> Utilitza la funció formatter i LocalDateTime per retornar la data en un format especificat. (yyyy-MM-dd)
* */
fun agafarDataActual():String {
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    return LocalDateTime.now().format(formatter)
}