import java.io.File
import kotlin.io.path.Path
import kotlin.io.path.appendText
import kotlin.io.path.notExists

/**
* crearUsuari -> Afegeix l'usuari especificat pel client amb input de teclat dins del fitxer d'usuaris (userData.txt)
* */
fun crearUsuari() {
    val outputPath = "./src/data/userData.txt"
    if(Path(outputPath).notExists()){
        File("./src/data/userData.txt").createNewFile()
    }
    //Introduir id
    if (File(outputPath).length() == 0L) { //Control de contingut a userdata.txt (si té o no contingut)
        File(outputPath).appendText(inserirNovaId() +", ")
    } else {
        File(outputPath).appendText("\n"+ inserirNovaId() +", ")
    }
    //Demanar nom, telefon i email
    print("Introdueixi el seu nom: ")
    Path(outputPath).appendText(readln()+", ")
    print("Introdueixi el seu telèfon: ")
    Path(outputPath).appendText(readln()+", ")
    print("Introdueixi el seu email: ")
    Path(outputPath).appendText(readln()+", ")
    //Per defecte posem que es actiu desbloquejat
    Path(outputPath).appendText("true"+", ")
    Path(outputPath).appendText("false"+";")

}
/**
* modificarUsuari -> Modifica un usuari ja existent a partir de la seva ID.
* */
fun modificarUsuari(){
    //Introduir id del usuari a modificar
    println("Introdueixi la id del usuari a modificar")
    val idAModificar = readln()
    if(comprovarID(idAModificar)){
        println("Escriu el nom de l'usuari que vols modificar")
        val nom = readln()
        modificarDades(1,idAModificar,nom)
        print("Telèfon:")
        val telefon = readln()
        modificarDades(2,idAModificar,telefon)
        print("Email:")
        val email = readln()
        modificarDades(3,idAModificar,email)
    }
}
/**
* alternarBloqueigUsuari -> Si l'usuari està bloquejat el desbloqueja, i si no esta bloquejat el bloqueja.
* */
fun alternarBloqueigUsuari(){
    //Introduir id del usuari a bloquejar o desbloquejar
    println("Introdueixi la id del usuari per a alternar el bloqueig")
    val idUsuari = readln()
    //Alternar Bloqueig
    alternarBloqueig(idUsuari)
}
/**
* sortir -> S'executa cada vegada que es prema la opció del menú sortir. Fa les funcions necessaries abans
* d'apagar el programa.
* */
fun sortir(){
    //Funcions a executar abans de sortir
    ferBackup() //NOTA: intellij triga una mica a indexar, per això pot ser que el fitxer es mostri després d'uns segons.
    //Sortir
    println("Sortint...")
}