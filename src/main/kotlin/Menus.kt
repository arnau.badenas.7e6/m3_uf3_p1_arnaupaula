/**
* menuPrincipal -> Selecció de menú, mostra el menú fins que l'usuari escull una opció correcta.
* */
fun menuPrincipal(){
    do {
        mostrarMenu()
        val opcio = readln()
        when(opcio){
            "1" -> crearUsuari()
            "2" -> modificarUsuari()
            "3" -> alternarBloqueigUsuari()
            "4" -> sortir()
            else -> println("Opció incorrecta!")
        }
    }while (opcio != "4")
}
fun mostrarMenu(){
    println("------GRUP ASMA - Interfície d'usuaris------\n" +
            "1. Crear usuari\n" +
            "2. Modificar usuari\n" +
            "3. Desbloquejar/Bloquejar usuari\n" +
            "4. Sortir\n" +
            "--------------------------------------------")
}