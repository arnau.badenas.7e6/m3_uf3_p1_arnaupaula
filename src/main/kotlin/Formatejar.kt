import java.io.File
import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.io.path.exists
import kotlin.io.path.notExists
import kotlin.io.path.readText
/**
 * Agafa l'última id en "idActual" i la suma.
 * Els que tenen true o false en algun dels dos camps actiu o block s'eliminaran permanentment.
 * @return Retorna un String de la id Nova.*/
fun inserirNovaId():String {
    var cont = 0
    val userDataFile = File("src/data/userData.txt")
    //Insereix text
    if (Path("src/data/userData.txt").exists()) {
        userDataFile.forEachLine {
            cont++
        }
    }
    cont+=1
    return cont.toString()
}
/**
 * eliminacioUsuaris -> Elimina els usuaris amb els camps d'actiu en false i bloquejat en true. Seguidament crea un fitxer nou si aquest
 * no existeix i els importa.*/
fun eliminacioUsuaris(path: Path) {
    val userDataPath = "src/data/userData.txt"
    val users = path.readText().split(";").toMutableList()

    for (i in users.indices){
        val user = users[i].split(",").toTypedArray()
        user[0] = ""
        //println(user.contentToString())

        if (user[4] == "false" && user[5] == "true") { //
            println("S'han carregat totes les dades. \n" +
                    "S'ha eliminat l'usuari: ${user[1]}")
        } else {
            val usuarisNouFormat = user.contentToString().replace("[","").replace("]","")
            if (Path(userDataPath).notExists()) { //Control d'existència de fitxer userdata
                File(userDataPath).createNewFile()
                File(userDataPath).appendText("${inserirNovaId()}${usuarisNouFormat};")
            } else {
                if (File(userDataPath).length() == 0L) { //Control de contingut a userdata.txt (si té o no contingut)
                    File(userDataPath).appendText("${inserirNovaId()}${usuarisNouFormat};")
                } else {
                    File(userDataPath).appendText("\n${inserirNovaId()}${usuarisNouFormat};")
                }
            }
        }
    }
}
/**
* modificarDades -> Recull totes les dades en una llista i introdueix les noves dades.
* */
fun modificarDades(posicio:Int, id:String, dadesNoves: String) {
    val userDataPath = "src/data/userData.txt"
    val users = Path(userDataPath).readText().replace("\n","").split(";").toMutableList()
    users.removeLast()
    //println(users)
    //Eliminem tot el text del fitxer per poder carregar-lo de nou posteriorment amb les noves dades.
    File(userDataPath).writeText("")
    for (i in users.indices){
        val user = users[i].split(",").toTypedArray()
        for (j in user.indices){
            user[j] = user[j].trim()
        }
        //println(user.contentToString())
        if (id == user[0]) { //Canviem les dades antigues per les noves insertades.
            user[posicio] = dadesNoves
        }
        val usuarisNovesDades = user.contentToString().replace("[", "").replace("]", "")
        //Torna a carregar totes les dades a userData.txt guardant tot a una nova variable
        if (File(userDataPath).length() == 0L) { //Control de contingut a userdata.txt (si té o no contingut)
            File(userDataPath).appendText("${usuarisNovesDades};")
        } else {
            File(userDataPath).appendText("\n${usuarisNovesDades};")
        }
    }

}
/**
 * alternarBloqueig -> alterna el camp de bloqueig de true a false i de false a true. Seguidament importa les dades actualitzades a
 * userData.txt */
fun alternarBloqueig(id:String) {
    val userDataPath = "src/data/userData.txt"
    val users = Path(userDataPath).readText().replace("\n", "").trim().split(";").toMutableList()
    users.removeLast()
    //println(users)
    if (comprovarID(id)) { //Control d'existència de la ID
        File(userDataPath).writeText("")
        for (i in users.indices) {
            val user = users[i].split(",").toTypedArray()
            for (j in user.indices){
                user[j] = user[j].trim()
            }
            //println(user.contentToString())
            //Corroborem l'id
            if (id == user[0]) {
                println("S'ha alternat bloqueig de: ${user[1]}")
                //Alternem el bloqueig
                if (user[5].contains("false")) {
                    user[5] = "true"
                } else {
                    user[5] = "false"
                }
            }
            //Torna a carregar totes les dades a userData.txt guardant tot a una nova variable
            val usuarisBloqueigAlternat = user.contentToString().replace("[", "").replace("]", "")
            if (File(userDataPath).length() == 0L) { //Control de contingut a userdata.txt (si té o no contingut)
                File(userDataPath).appendText("${usuarisBloqueigAlternat};")
            } else {
                File(userDataPath).appendText("\n${usuarisBloqueigAlternat};")
            }
        }
    }
}
/**
 * comprovarID -> comprova si la id del usuari esta dins del fitxer userData.txt
 * @return Torna true si exsisteix, false si no exsisteix */
fun comprovarID(id:String):Boolean { //Control d'existència de id.
    val userDataPath = "src/data/userData.txt"
    val users = Path(userDataPath).readText().replace("\n", "").trim().split(";").toMutableList()
    for (i in users.indices) {
        val user = users[i].split(",").toTypedArray()
        //Corroborem l'id
        if (id == user[0]) {
            return true
        }
    }
    println("L'id: $id no existeix.")
    return false
}
