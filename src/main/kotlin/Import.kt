import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.io.path.listDirectoryEntries
import kotlin.io.path.notExists

/** Comprova si existeix un nou fitxer en el directori d'import.
En cas que existeixi haurà de realitzar una lectura d’aquest i processar les dades */
fun comprovarExistencia() {
    val path = Path("./src/import/")
    if(path.notExists()){
        File("./src/import").mkdir()
    }
    val files : List<Path> = path.listDirectoryEntries()
    if (files.isNotEmpty()){
        carregarUsuaris(path)
    } else println("No hi ha ninguna dada a carregar ")

}
/*### IMPORTACIÓ ###*/
/** carregaUsuaris -> Carrega els usuaris (menys la ID) en un nou fitxer al directori de data. */
fun carregarUsuaris(path: Path) {
    val files : List<Path> = path.listDirectoryEntries()
    for(item in files) {
        eliminacioUsuaris(item)
        eliminarFitxer(item)
    }
}
/** eliminarFitxer -> Eliminarà el fitxer un cop guardades les dades. */
fun eliminarFitxer(item: Path) {
    val eliminar = Files.deleteIfExists(item)
    if (eliminar) {
        println("S'ha eliminat el fitxer exportat")
    } else { println("ERROR") }
}



