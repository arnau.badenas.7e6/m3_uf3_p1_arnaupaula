# SISTEMA DE FITXERS per a ASMA

El sistema de fitxer d'Asma serveix per importar dades antigues, formatejar-les amb un nou estil més entenedor i fer backups de forma automàtica.

## Estructura del projecte
El programa compta amb 3 directoris.
- import: Aqui dins van els fitxers de dades a importar.
- data: Aqui van els fitxers ja formatejats.
- backups: Aqui es guarden els backups dels fitxers de dades d'usuaris amb el format (yyyy-mm-ddUserdata.txt)

## Ús
Al obrir el programa s'importen les dades automàticament. Si no hi ha res mostra el comentari. Seguidament, mostra el menú:
```bash
No hi ha ninguna dada a carregar 
------GRUP ASMA - Interfície d'usuaris------
1. Crear usuari
2. Modificar usuari
3. Desbloquejar/Bloquejar usuari
4. Sortir
--------------------------------------------
```
Es contemplen les opcions:
- Crear usuari: Es crea un usuari
```bash
Introdueixi el seu nom: Hola
Introdueixi el seu telèfon: Prova
Introdueixi el seu email: Prova
```
Això crea un usuari al arxiu userData.txt dins de data. Per defecte està actiu i no bloquejat.

Aqui es poden comprovar unes proves amb el format indicat.
```bash
1, Carla Marlina, 654824951, marlina.87@mail.cat, true, false;
2, Àngel Sanz, 697543215, angel.sanz@mail.com, true, false;
3, Lara Gomez, 689953247, lara1985@mail.com, true, true;
4, Raül Lopez, 634894069, raul1602@mail.com, false, false;
5, Antonio Gonzalez, 657094725, antonio.gon.1986@mail.es, true, false;
6, Nerea Perez, 678537121, nerea.1958@mail.com, true, false;
7, Laura Gimenez, 689521475, lauritax12@mail.es, true, false;
8, Nom, Telefon, Email, true, false;
9, Hola, Prova, Prova, true, false;
```
- Modificar Usuari: El programa demana la id a modificar i seguidament prompta al usuari a introduir els valors.
```bash
------GRUP ASMA - Interfície d'usuaris------
1. Crear usuari
2. Modificar usuari
3. Desbloquejar/Bloquejar usuari
4. Sortir
--------------------------------------------
2
Introdueixi la id del usuari a modificar
9
Escriu el nom de l'usuari que vols modificar
NomModificat
Telèfon:TelefonModificat123123
Email:EmailModificat@gmail.com
```
I el resultat queda així:

```bash
9, NomModificat, TelefonModificat123123, EmailModificat@gmail.com, true, false;
```
- Desbloquejar/Bloquejar Usuari: Alterna el bloqueig del usuari especificat.

```bash
------GRUP ASMA - Interfície d'usuaris------
1. Crear usuari
2. Modificar usuari
3. Desbloquejar/Bloquejar usuari
4. Sortir
--------------------------------------------
3
Introdueixi la id del usuari per a alternar el bloqueig
9
S'ha alternat bloqueig de: NomModificat
```
I el resultat queda així. (s'altera el 6é camp de false a true o de true a false)

```bash
9, NomModificat, TelefonModificat123123, EmailModificat@gmail.com, true, true;
```

## Contribuidors

**Arnau Badenas Vives**: Opcions i menús

**Paula Garcia Dopico**: Importació i format

## Llicència

[Tots els drets reservats a ASMA](https://i.kym-cdn.com/photos/images/facebook/001/822/061/f9a.jpg)
